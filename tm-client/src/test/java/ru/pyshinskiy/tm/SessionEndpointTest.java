package ru.pyshinskiy.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.pyshinskiy.tm.api.endpoint.ISessionEndpoint;
import ru.pyshinskiy.tm.api.endpoint.IUserEndpoint;
import ru.pyshinskiy.tm.api.endpoint.SessionDTO;
import ru.pyshinskiy.tm.endpoint.SessionEndpointService;
import ru.pyshinskiy.tm.endpoint.UserEndpointService;

@Category(ru.pyshinskiy.tm.SessionEndpointTestIntegrate.class)
public class SessionEndpointTest extends Assert {

    @Nullable
    private ISessionEndpoint sessionEndpoint;

    @Nullable
    private IUserEndpoint userEndpoint;

    @Before
    public void setUp() {
        sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();
        userEndpoint = new UserEndpointService().getUserEndpointPort();
    }

    @After
    public void tearDown() throws Exception {
        @NotNull final SessionDTO sessionDTO = sessionEndpoint.createSession("test", "1234");
        @NotNull final SessionDTO sessionDTOAdmin = sessionEndpoint.createSession("admin", "1234");
        @NotNull final SessionDTO sessionDTO2 = sessionEndpoint.createSession("test2", "1234");
        sessionEndpoint.removeAllSessionsByUserId(sessionDTO.getUserId());
        sessionEndpoint.removeAllSessionsByUserId(sessionDTOAdmin.getUserId());
        sessionEndpoint.removeAllSessionsByUserId(sessionDTO2.getUserId());
    }

    @Test
    public void createSession() throws Exception {
        @NotNull final SessionDTO sessionDTO = sessionEndpoint.createSession("test", "1234");
        assertTrue(sessionEndpoint.isSessionExist(sessionDTO.getId()));
    }

    @Test
    public void updateSession() throws Exception {
        @NotNull final SessionDTO sessionDTO = sessionEndpoint.createSession("admin", "1234");
        @NotNull final SessionDTO updatedSessionDTO = sessionEndpoint.updateSession(sessionDTO);
        assertNotEquals(sessionDTO.getTimestamp(), updatedSessionDTO.getTimestamp());
    }

    @Test
    public void removeSession() throws Exception {
        @Nullable final SessionDTO sessionDTO = sessionEndpoint.createSession("test", "1234");
        sessionEndpoint.removeSession(sessionDTO.getUserId(), sessionDTO.getId());
        assertFalse(sessionEndpoint.isSessionExist(sessionDTO.getId()));
    }

    /*-------------------------security--------------------------*/

    @Test
    public void removeSessionWithInvalidUserId() throws Exception {
        @NotNull final SessionDTO sessionDTO = sessionEndpoint.createSession("test", "1234");
        @NotNull final SessionDTO sessionDTO2 = sessionEndpoint.createSession("test2", "1234");
        sessionEndpoint.removeSession(sessionDTO2.getUserId(), sessionDTO.getId());
        assertTrue(sessionEndpoint.isSessionExist(sessionDTO.getId()));
    }
}
