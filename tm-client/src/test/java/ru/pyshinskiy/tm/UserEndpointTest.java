package ru.pyshinskiy.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.pyshinskiy.tm.api.endpoint.*;
import ru.pyshinskiy.tm.endpoint.SessionEndpointService;
import ru.pyshinskiy.tm.endpoint.UserEndpointService;

import java.lang.Exception;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Category(ru.pyshinskiy.tm.UserEndpointTestIntegrate.class)
public class UserEndpointTest extends Assert {

    @Nullable
    private ISessionEndpoint sessionEndpoint;

    @Nullable
    private IUserEndpoint userEndpoint;

    @Nullable
    private SessionDTO sessionDTOUser;

    @Nullable
    private SessionDTO sessionDTOAdmin;

    @Before
    public void setUp() throws Exception {
        sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();
        userEndpoint = new UserEndpointService().getUserEndpointPort();
        sessionDTOUser = sessionEndpoint.createSession("test", "1234");
        sessionDTOAdmin = sessionEndpoint.createSession("admin", "1234");
    }

    @After
    public void tearDown() throws Exception {
        for(@NotNull final UserDTO userDTO : userEndpoint.findAllUsers(sessionDTOAdmin)) {
            if(userDTO.getRole().equals(Role.TEST)) userEndpoint.removeUser(sessionDTOAdmin, userDTO.getId());
        }
        sessionEndpoint.removeAllSessionsByUserId(sessionDTOUser.getUserId());
        sessionEndpoint.removeAllSessionsByUserId(sessionDTOAdmin.getUserId());
    }

    @Test
    public void createUser() throws Exception {
        @NotNull final UserDTO userDTO = createUserDTO();
        userEndpoint.persistUser(userDTO);
        assertEquals(userDTO.getId(), userEndpoint.findOneUser(sessionDTOUser, userDTO.getId()).getId());
    }

    @Test
    public void findAllUsers() throws Exception {
        @NotNull final List<UserDTO> users = new ArrayList<>();
        for(int i = 0; i < 5; i++) {
            @NotNull final UserDTO userDTO = createUserDTO();
            users.add(userDTO);
            userEndpoint.persistUser(userDTO);
        }
        assertTrue(users.size() <= userEndpoint.findAllUsers(sessionDTOAdmin).size());
    }

    @Test
    public void mergerUser() throws Exception {
        @NotNull final UserDTO userDTO = createUserDTO();
        userDTO.setLogin("editLogin");
        userEndpoint.mergeUser(sessionDTOUser, userDTO);
        assertEquals(userDTO.getLogin(), userEndpoint.findOneUser(sessionDTOUser, userDTO.getId()).getLogin());
    }

    @Test
    public void removeUser() throws Exception {
        @NotNull final UserDTO userDTO = createUserDTO();
        userEndpoint.persistUser(userDTO);
        userEndpoint.removeUser(sessionDTOAdmin, userDTO.getId());
        assertNull(userEndpoint.findOneUser(sessionDTOUser, userDTO.getId()));
    }

    /*--------------------------security-----------------------------*/

    @Test(expected = Exception_Exception.class)
    public void removeUserByInvalidSession() throws Exception {
        userEndpoint.removeUser(sessionDTOUser, sessionDTOUser.getId());
    }

    @Test(expected = Exception_Exception.class)
    public void removeAllUsersByInvalidSession() throws Exception {
        userEndpoint.removeAllUsers(sessionDTOUser);
    }

    @NotNull
    private UserDTO createUserDTO() {
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setLogin("user" + new Random().toString());
        userDTO.setPasswordHash("1234");
        userDTO.setRole(Role.TEST);
        return userDTO;
    }
}
