package ru.pyshinskiy.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.service.CurrentSessionService;
import ru.pyshinskiy.tm.service.TerminalService;

@Component
public abstract class AbstractCommand {

    @Autowired
    @Nullable
    protected TerminalService terminalService;

    @Autowired
    @Nullable
    protected CurrentSessionService currentSessionService;

    public boolean isAllowed() {
        return currentSessionService.getSessionDTO() != null;
    }

    @NotNull
    public abstract String command();

    @NotNull
    public abstract String description();

    public abstract void execute() throws Exception;
}
