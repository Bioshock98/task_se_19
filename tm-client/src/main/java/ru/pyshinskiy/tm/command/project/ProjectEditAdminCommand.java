package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ProjectDTO;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.command.AbstractCommand;

import java.util.List;

import static ru.pyshinskiy.tm.util.date.DateUtil.parseDateFromString;
import static ru.pyshinskiy.tm.util.date.DateUtil.toXMLGregorianCalendar;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

@Component
@Qualifier("project_edit_admin")
public final class ProjectEditAdminCommand extends AbstractCommand {

    @Autowired
    @Nullable
    private IProjectEndpoint projectEndpoint;

    @Override
    public boolean isAllowed() {
        if (currentSessionService.getSessionDTO() == null) return false;
        return currentSessionService.getSessionDTO().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "project_edit_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "edit any project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT EDIT]");
        System.out.println("ENTER PROJECT ID");
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findAllProjects(currentSessionService.getSessionDTO());
        printProjects(projects);
        final int projectNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String projectId = projects.get(projectNumber).getId();
        @Nullable final ProjectDTO project = projectEndpoint.findOneProject(currentSessionService.getSessionDTO(), projectId);
        @NotNull final ProjectDTO anotherProject = new ProjectDTO();
        anotherProject.setUserId(project.getUserId());
        System.out.println("ENTER NAME");
        anotherProject.setName(terminalService.nextLine());
        anotherProject.setId(project.getId());
        System.out.println("ENTER PROJECT DESCRIPTION");
        anotherProject.setDescription(terminalService.nextLine());
        System.out.println("ENTER START DATE");
        anotherProject.setStartDate(toXMLGregorianCalendar(parseDateFromString(terminalService.nextLine())));
        System.out.println("ENTER FINISH DATE");
        anotherProject.setFinishDate(toXMLGregorianCalendar(parseDateFromString(terminalService.nextLine())));
        projectEndpoint.mergeProject(currentSessionService.getSessionDTO(), anotherProject);
        System.out.println("[OK]");
    }
}
