package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.TaskDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.date.DateUtil.parseDateFromString;
import static ru.pyshinskiy.tm.util.date.DateUtil.toXMLGregorianCalendar;

@Component
@Qualifier("task_create")
public final class TaskCreateCommand extends AbstractCommand {

    @Autowired
    @Nullable
    private ITaskEndpoint taskEndpoint;

    @Override
    @NotNull
    public String command() {
        return "task_create";
    }

    @Override
    @NotNull
    public String description() {
        return "create new task";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CREATE]");
        System.out.println("[ENTER TASK NAME]");
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(currentSessionService.getSessionDTO().getUserId());
        task.setName(terminalService.nextLine());
        System.out.println("ENTER TASK DESCRIPTION");
        task.setDescription(terminalService.nextLine());
        System.out.println("ENTER START DATE");
        task.setStartDate(toXMLGregorianCalendar(parseDateFromString(terminalService.nextLine())));
        System.out.println("ENTER END DATE");
        task.setFinishDate(toXMLGregorianCalendar(parseDateFromString(terminalService.nextLine())));
        taskEndpoint.persistTask(currentSessionService.getSessionDTO(), task);
        System.out.println("[OK]");
    }
}
