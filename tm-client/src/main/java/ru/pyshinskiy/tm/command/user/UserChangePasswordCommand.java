package ru.pyshinskiy.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.IUserEndpoint;
import ru.pyshinskiy.tm.api.endpoint.UserDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

@Component
@Qualifier("user_change_password")
public final class UserChangePasswordCommand extends AbstractCommand {

    @Autowired
    @Nullable
    private IUserEndpoint userEndpoint;

    @Override
    @NotNull
    public String command() {
        return "user_change_password";
    }

    @Override
    @NotNull
    public String description() {
        return "change user password";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER CHANGE PASSWORD");
        System.out.println("ENTER NEW PASSWORD");
        @NotNull final UserDTO user = new UserDTO();
        user.setId(currentSessionService.getSessionDTO().getUserId());
        user.setPasswordHash(terminalService.nextLine());
        userEndpoint.mergeUser(currentSessionService.getSessionDTO(), user);
        System.out.println("[OK]");
    }
}
