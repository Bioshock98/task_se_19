package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ProjectDTO;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.command.AbstractCommand;

import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProject;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

@Component
@Qualifier("project_select_admin")
public final class ProjectSelectAdminCommand extends AbstractCommand {

    @Autowired
    @Nullable
    private IProjectEndpoint projectEndpoint;

    @Override
    public boolean isAllowed() {
        if (currentSessionService.getSessionDTO() == null) return false;
        return currentSessionService.getSessionDTO().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "project_select_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "show any user project info";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT SELECT]");
        System.out.println("ENTER PROJECT ID");
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findAllProjects(currentSessionService.getSessionDTO());
        printProjects(projects);
        final int projectNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String projectId = projects.get(projectNumber).getId();
        @Nullable final ProjectDTO project = projectEndpoint.findOneProject(currentSessionService.getSessionDTO(), projectId);
        if (project == null) {
            throw new Exception("project doesn't exist");
        }
        printProject(project);
        System.out.println("[OK]");
    }
}
