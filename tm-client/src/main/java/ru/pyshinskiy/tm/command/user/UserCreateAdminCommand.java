package ru.pyshinskiy.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.IUserEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.api.endpoint.UserDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

@Component
@Qualifier("user_create_admin")
public final class UserCreateAdminCommand extends AbstractCommand {

    @Autowired
    @Nullable
    private IUserEndpoint userEndpoint;

    @Override
    public boolean isAllowed() {
        if (currentSessionService.getSessionDTO() == null) return false;
        return currentSessionService.getSessionDTO().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "user_create_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "create a new user or administrator";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER CREATE]");
        System.out.println("ENTER ROLE");
        @NotNull final Role role = Role.valueOf(terminalService.nextLine().toUpperCase());
        System.out.println("ENTER USERNAME");
        @NotNull final String login = terminalService.nextLine();
        System.out.println("ENTER PASSWORD");
        @NotNull final String password = terminalService.nextLine();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setRole(role);
        userEndpoint.persistUser(user);
        System.out.println("[OK]");
    }
}
