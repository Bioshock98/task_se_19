package ru.pyshinskiy.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.ISessionEndpoint;
import ru.pyshinskiy.tm.api.endpoint.SessionDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

@Component
@Qualifier("user_log_out")
public final class UserLogOutCommand extends AbstractCommand {

    @Autowired
    @Nullable
    private ISessionEndpoint sessionEndpoint;

    @Override
    @NotNull
    public String command() {
        return "user_log_out";
    }

    @Override
    @NotNull
    public String description() {
        return "log out";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("USER LOG OUT");
        @Nullable final SessionDTO currentSession = currentSessionService.getSessionDTO();
        sessionEndpoint.removeSession(currentSession.getUserId(), currentSession.getId());
        currentSessionService.setSessionDTO(null);
        System.out.println("[OK]");
    }
}
