package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.command.AbstractCommand;

@Component
@Qualifier("project_clear")
public final class ProjectClearCommand extends AbstractCommand {

    @Autowired
    @Nullable
    private IProjectEndpoint projectEndpoint;

    @Override
    @NotNull
    public String command() {
        return "project_clear";
    }

    @Override
    @NotNull
    public String description() {
        return "remove all projects";
    }

    @Override
    public void execute() throws Exception {
        projectEndpoint.removeAllProjectsByUserId(currentSessionService.getSessionDTO());
        System.out.println("[ALL PROJECTS REMOVED]");
    }
}
