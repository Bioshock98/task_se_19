package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

@Component
@Qualifier("task_find_by_name")
public final class TaskFindByNameCommand extends AbstractCommand {

    @Autowired
    @Nullable
    private ITaskEndpoint taskEndpoint;

    @Override
    @NotNull
    public String command() {
        return "task_find_by_name";
    }

    @Override
    @NotNull
    public String description() {
        return "find task by name";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK FIND BY NAME");
        System.out.println("ENTER TASK NAME");
        @NotNull final String name = terminalService.nextLine();
        printTasks(taskEndpoint.findTasksByName(currentSessionService.getSessionDTO(), name));
        System.out.println("[OK]");
    }
}
