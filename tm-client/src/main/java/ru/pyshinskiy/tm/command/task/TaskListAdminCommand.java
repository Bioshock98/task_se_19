package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.api.endpoint.SessionDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

@Component
@Qualifier("task_list_admin")
public final class TaskListAdminCommand extends AbstractCommand {

    @Autowired
    @Nullable
    private ITaskEndpoint taskEndpoint;

    @Override
    public boolean isAllowed() {
        if (currentSessionService.getSessionDTO() == null) return false;
        return currentSessionService.getSessionDTO().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "task_list_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "list all users tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST ADMIN]");
        System.out.println("CHOOSE SORT TYPE");
        System.out.println("[createTime, startDate, endDate, status]");
        @NotNull final String option = terminalService.nextLine();
        @NotNull final SessionDTO session = currentSessionService.getSessionDTO();
        switch (option) {
            case "createTime":
                printTasks(taskEndpoint.sortTasksByCreateTime(currentSessionService.getSessionDTO(), session.getUserId(), 1));
                break;
            case "startDate":
                printTasks(taskEndpoint.sortTasksByStartDate(currentSessionService.getSessionDTO(), session.getUserId(), 1));
                break;
            case "finishDate":
                printTasks(taskEndpoint.sortTasksByFinishDate(currentSessionService.getSessionDTO(), session.getUserId(), 1));
                break;
            case "status":
                printTasks(taskEndpoint.sortTasksByStatus(currentSessionService.getSessionDTO(), session.getUserId(), 1));
        }
        System.out.println("[OK]");
    }
}
