package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.api.endpoint.TaskDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTask;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

@Component
@Qualifier("task_select_admin")
public final class TaskSelectAdminCommand extends AbstractCommand {

    @Autowired
    @Nullable
    private ITaskEndpoint taskEndpoint;

    @Override
    public boolean isAllowed() {
        if (currentSessionService.getSessionDTO() == null) return false;
        return currentSessionService.getSessionDTO().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "task_select_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "show any user selected task info";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK SELECT]");
        System.out.println("ENTER TASK ID");
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findAllTasks(currentSessionService.getSessionDTO());
        printTasks(tasks);
        final int taskNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String taskId = tasks.get(taskNumber).getId();
        @Nullable final TaskDTO task = taskEndpoint.findOneTask(currentSessionService.getSessionDTO(), taskId);
        if (task == null) {
            throw new Exception("task doesn't exist");
        }
        printTask(task);
        System.out.println("[OK]");
    }
}
