package ru.pyshinskiy.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.ISessionEndpoint;
import ru.pyshinskiy.tm.api.endpoint.SessionDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.daemon.SessionUpdater;

@Component
@Qualifier("user_log_in")
public final class UserLogInCommand extends AbstractCommand {

    @Autowired
    @Nullable
    private ISessionEndpoint sessionEndpoint;

    @Autowired
    @Nullable
    private SessionUpdater sessionUpdater;

    @Override
    public boolean isAllowed() {
        return true;
    }

    @Override
    @NotNull
    public String command() {
        return "user_log_in";
    }

    @Override
    @NotNull
    public String description() {
        return "log in";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOG IN]");
        System.out.println("ENTER LOGIN");
        @NotNull String login = terminalService.nextLine();
        System.out.println("ENTER PASSWORD");
        @NotNull String password = terminalService.nextLine();
        @Nullable SessionDTO session = sessionEndpoint.createSession(login, password);
        while (session == null) {
            System.out.println("Invalid login or password");
            System.out.println("ENTER LOGIN");
            login = terminalService.nextLine();
            System.out.println("ENTER PASSWORD");
            password = terminalService.nextLine();
            session = sessionEndpoint.createSession(login, password);
        }
        @Nullable final SessionDTO currentSession = currentSessionService.getSessionDTO();
        if (currentSession != null) sessionEndpoint.removeSession(currentSession.getUserId(), currentSession.getId());
        currentSessionService.setSessionDTO(session);
        sessionUpdater.setDaemon(true);
        sessionUpdater.start();
        System.out.println("[OK]");
    }
}
