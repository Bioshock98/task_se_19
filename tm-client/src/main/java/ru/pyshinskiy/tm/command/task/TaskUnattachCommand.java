package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.TaskDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

@Component
@Qualifier("task_unattach")
public final class TaskUnattachCommand extends AbstractCommand {

    @Autowired
    @Nullable
    private ITaskEndpoint taskEndpoint;

    @Override
    @NotNull
    public String command() {
        return "task_unattach";
    }

    @Override
    @NotNull
    public String description() {
        return "unattach task from project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UNATTACH TASK]");
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findAllTasksByUserId(currentSessionService.getSessionDTO());
        printTasks(tasks);
        final int taskNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String taskId = tasks.get(taskNumber).getId();
        @Nullable final TaskDTO task = taskEndpoint.findOneTaskByUserId(currentSessionService.getSessionDTO(), taskId);
        if (task == null) {
            throw new Exception("task doesn't exist");
        }
        task.setProjectId(null);
        taskEndpoint.mergeTask(currentSessionService.getSessionDTO(), task);
        System.out.println("[OK]");
    }
}
