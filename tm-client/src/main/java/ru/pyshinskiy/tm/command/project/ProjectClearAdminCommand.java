package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.command.AbstractCommand;

@Component
@Qualifier("project_clear_admin")
public final class ProjectClearAdminCommand extends AbstractCommand {

    @Autowired
    @Nullable
    private IProjectEndpoint projectEndpoint;

    @Override
    public boolean isAllowed() {
        if (currentSessionService.getSessionDTO() == null) return false;
        return currentSessionService.getSessionDTO().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "project_clear_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "remove all users projects";
    }

    @Override
    public void execute() throws Exception {
        projectEndpoint.removeAllProjects(currentSessionService.getSessionDTO());
        System.out.println("[ALL PROJECTS REMOVED]");
    }
}
