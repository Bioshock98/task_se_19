package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.*;
import ru.pyshinskiy.tm.command.AbstractCommand;

import java.lang.Exception;
import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

@Component
@Qualifier("task_list_by_project_admin")
public final class TaskListByProjectAdminCommand extends AbstractCommand {

    @Autowired
    @Nullable
    private ITaskEndpoint taskEndpoint;

    @Autowired
    @Nullable
    private IProjectEndpoint projectEndpoint;

    @Override
    public boolean isAllowed() {
        if (currentSessionService.getSessionDTO() == null) return false;
        return currentSessionService.getSessionDTO().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "task_list_by_project_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "list tasks by any user project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST BY PROJECT]");
        System.out.println("[ENTER PROJECT ID");
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findAllProjects(currentSessionService.getSessionDTO());
        printProjects(projects);
        final int projectNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String projectId = projects.get(projectNumber).getId();
        @NotNull final List<TaskDTO> tasksByProjectId = taskEndpoint.findTasksByProjectId(currentSessionService.getSessionDTO(), projectId);
        printTasks(tasksByProjectId);
        System.out.println("[OK]");
    }
}
