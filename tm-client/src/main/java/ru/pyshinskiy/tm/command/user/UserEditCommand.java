package ru.pyshinskiy.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.IUserEndpoint;
import ru.pyshinskiy.tm.api.endpoint.UserDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

@Component
@Qualifier("user_edit")
public final class UserEditCommand extends AbstractCommand {

    @Autowired
    @Nullable
    private IUserEndpoint userEndpoint;

    @Override
    @NotNull
    public String command() {
        return "user_edit";
    }

    @Override
    @NotNull
    public String description() {
        return "edit existing user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER EDIT]");
        System.out.println("ENTER NEW USERNAME");
        @NotNull final UserDTO user = new UserDTO();
        user.setId(currentSessionService.getSessionDTO().getUserId());
        user.setLogin(terminalService.nextLine());
        System.out.println("ENTER NEW PASSWORD");
        user.setPasswordHash(terminalService.nextLine());
        userEndpoint.mergeUser(currentSessionService.getSessionDTO(), user);
        System.out.println("[OK]");
    }
}
