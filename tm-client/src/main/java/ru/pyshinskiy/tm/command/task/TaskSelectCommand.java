package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.TaskDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTask;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

@Component
@Qualifier("task_select")
public final class TaskSelectCommand extends AbstractCommand {

    @Autowired
    @Nullable
    private ITaskEndpoint taskEndpoint;

    @Override
    @NotNull
    public String command() {
        return "task_select";
    }

    @Override
    @NotNull
    public String description() {
        return "show task info";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK SELECT]");
        System.out.println("ENTER TASK ID");
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findAllTasksByUserId(currentSessionService.getSessionDTO());
        printTasks(tasks);
        final int taskNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String taskId = tasks.get(taskNumber).getId();
        @Nullable final TaskDTO task = taskEndpoint.findOneTaskByUserId(currentSessionService.getSessionDTO(), taskId);
        if (task == null) {
            throw new Exception("task doesn't exist");
        }
        printTask(task);
        System.out.println("[OK]");
    }
}
