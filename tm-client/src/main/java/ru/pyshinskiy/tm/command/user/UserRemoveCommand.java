package ru.pyshinskiy.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.IUserEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.api.endpoint.UserDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printUsers;

@Component
@Qualifier("user_remove")
public final class UserRemoveCommand extends AbstractCommand {

    @Autowired
    @Nullable
    private IUserEndpoint userEndpoint;

    @Override
    public boolean isAllowed() {
        if (currentSessionService.getSessionDTO() == null) return false;
        return currentSessionService.getSessionDTO().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "user_remove";
    }

    @Override
    @NotNull
    public String description() {
        return "remove existing user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("ENTER USER ID");
        @NotNull final List<UserDTO> users = userEndpoint.findAllUsers(currentSessionService.getSessionDTO());
        printUsers(users);
        final int userNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String userId = users.get(userNumber).getId();
        userEndpoint.removeUser(currentSessionService.getSessionDTO(), userId);
        System.out.println("USER REMOVED");
    }
}
