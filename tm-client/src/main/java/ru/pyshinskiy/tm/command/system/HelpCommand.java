package ru.pyshinskiy.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.command.AbstractCommand;

import java.util.Collection;

@Component
@Qualifier("help")
public final class HelpCommand extends AbstractCommand {

    @Nullable
    private Collection<AbstractCommand> abstractCommands;

    public void setAbstractCommands(@NotNull final Collection<AbstractCommand> abstractCommands) {
        this.abstractCommands = abstractCommands;
    }

    @Override
    public boolean isAllowed() {
        return true;
    }

    @Override
    @NotNull
    public String command() {
        return "help";
    }

    @Override
    @NotNull
    public String description() {
        return "list available commands";
    }

    @Override
    public void execute() {
        for (@NotNull final AbstractCommand command : abstractCommands) {
            if (command.isAllowed()) {
                System.out.println(command.command() + ": " +
                        command.description());
            }
        }
        System.out.println("[OK]");
    }
}
