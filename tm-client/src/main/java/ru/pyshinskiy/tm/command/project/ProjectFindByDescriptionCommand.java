package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

@Component
@Qualifier("project_find_by_description")
public final class ProjectFindByDescriptionCommand extends AbstractCommand {

    @Autowired
    @Nullable
    private IProjectEndpoint projectEndpoint;

    @Override
    @NotNull
    public String command() {
        return "project_find_by_description";
    }

    @Override
    @NotNull
    public String description() {
        return "find project by its description";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT FIND BY DESCRIPTION]");
        System.out.println("ENTER PROJECT DESCRIPTION");
        @NotNull final String description = terminalService.nextLine();
        printProjects(projectEndpoint.findProjectByDescription(currentSessionService.getSessionDTO(), description));
        System.out.println("[OK]");
    }
}
