package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.api.endpoint.SessionDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

@Component
@Qualifier("project_list_admin")
public final class ProjectListAdminCommand extends AbstractCommand {

    @Autowired
    @Nullable
    private IProjectEndpoint projectEndpoint;

    @Override
    public boolean isAllowed() {
        if (currentSessionService.getSessionDTO() == null) return false;
        return currentSessionService.getSessionDTO().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "project_list_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "list all users projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST]");
        System.out.println("DO YOU WANT TO SORT PROJECTS?");
        @NotNull final String doSort = terminalService.nextLine();
        @NotNull final SessionDTO session = currentSessionService.getSessionDTO();
        if ("y".equals(doSort) || "yes".equals(doSort)) {
            System.out.println("CHOOSE SORT TYPE");
            System.out.println("[createTime, startDate, finishDate, status]");
            @NotNull final String option = terminalService.nextLine();
            switch (option) {
                case "createTime":
                    printProjects(projectEndpoint.sortProjectsByCreateTime(currentSessionService.getSessionDTO(), session.getUserId(), 1));
                    break;
                case "startDate":
                    printProjects(projectEndpoint.sortProjectsByStartDate(currentSessionService.getSessionDTO(), session.getUserId(), 1));
                    break;
                case "finishDate":
                    printProjects(projectEndpoint.sortProjectsByFinishDate(currentSessionService.getSessionDTO(), session.getUserId(), 1));
                    break;
                case "status":
                    printProjects(projectEndpoint.sortProjectsByStatus(currentSessionService.getSessionDTO(), session.getUserId(), 1));
            }
            System.out.println("[OK]");
        } else {
            printProjects(projectEndpoint.findAllProjects(currentSessionService.getSessionDTO()));
            System.out.println("[OK]");
        }
    }
}
