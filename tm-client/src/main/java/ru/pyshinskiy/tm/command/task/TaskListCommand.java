package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.SessionDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

@Component
@Qualifier("task_list")
public final class TaskListCommand extends AbstractCommand {

    @Autowired
    @Nullable
    private ITaskEndpoint taskEndpoint;

    @Override
    @NotNull
    public String command() {
        return "task_list";
    }

    @Override
    @NotNull
    public String description() {
        return "show all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST]");
        System.out.println("CHOOSE SORT TYPE");
        System.out.println("[createTime, startDate, endDate, status]");
        @NotNull final String option = terminalService.nextLine();
        @NotNull final SessionDTO sessionDTO = currentSessionService.getSessionDTO();
        switch (option) {
            case "createTime":
                printTasks(taskEndpoint.sortTasksByCreateTime(currentSessionService.getSessionDTO(), sessionDTO.getUserId(), 1));
                break;
            case "startDate":
                printTasks(taskEndpoint.sortTasksByStartDate(currentSessionService.getSessionDTO(), sessionDTO.getUserId(), 1));
                break;
            case "finishDate":
                printTasks(taskEndpoint.sortTasksByFinishDate(currentSessionService.getSessionDTO(), sessionDTO.getUserId(), 1));
                break;
            case "status":
                printTasks(taskEndpoint.sortTasksByStatus(currentSessionService.getSessionDTO(), sessionDTO.getUserId(), 1));
        }
        System.out.println("[OK]");
    }
}
