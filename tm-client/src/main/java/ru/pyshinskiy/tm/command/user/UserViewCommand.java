package ru.pyshinskiy.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.IUserEndpoint;
import ru.pyshinskiy.tm.command.AbstractCommand;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printUser;

@Component
@Qualifier("user_view")
public final class UserViewCommand extends AbstractCommand {

    @Autowired
    @Nullable
    private IUserEndpoint userEndpoint;

    @Override
    @NotNull
    public String command() {
        return "user_view";
    }

    @Override
    @NotNull
    public String description() {
        return "show user info";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("USER VIEW");
        @NotNull final String userId = currentSessionService.getSessionDTO().getUserId();
        printUser(userEndpoint.findOneUser(currentSessionService.getSessionDTO(), userId));
        System.out.println("[OK]");
    }
}
