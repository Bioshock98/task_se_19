package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.command.AbstractCommand;

@Component
@Qualifier("task_clear")
public final class TaskClearCommand extends AbstractCommand {

    @Autowired
    @Nullable
    private ITaskEndpoint taskEndpoint;

    @Override
    @NotNull
    public String command() {
        return "task_clear";
    }

    @Override
    @NotNull
    public String description() {
        return "remove all tasks";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CLEAR TASK]");
        taskEndpoint.removeAllTasksByUserId(currentSessionService.getSessionDTO());
        System.out.println("[ALL TASKS REMOVED]");
    }
}
