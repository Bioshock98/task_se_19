package ru.pyshinskiy.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.command.AbstractCommand;

@Component
@Qualifier("about")
public final class AboutCommand extends AbstractCommand {

    @Override
    public boolean isAllowed() {
        return true;
    }

    @Override
    @NotNull
    public String command() {
        return "about";
    }

    @Override
    @NotNull
    public String description() {
        return "print build number info";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final String buildNumber = Manifests.read("buildNumber");
        @NotNull final String developer = Manifests.read("developer");
        System.out.println("build number: " + buildNumber + "\n" + "developer: " + developer);
    }
}
