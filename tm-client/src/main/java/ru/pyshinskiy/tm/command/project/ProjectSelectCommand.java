package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ProjectDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProject;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

@Component
@Qualifier("project_select")
public final class ProjectSelectCommand extends AbstractCommand {

    @Autowired
    @Nullable
    private IProjectEndpoint projectEndpoint;

    @Override
    @NotNull
    public String command() {
        return "project_select";
    }

    @Override
    @NotNull
    public String description() {
        return "show project info";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT SELECT]");
        System.out.println("ENTER PROJECT ID");
        @NotNull final List<ProjectDTO> projects = projectEndpoint.findAllProjectsByUserId(currentSessionService.getSessionDTO());
        printProjects(projects);
        final int projectNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String projectId = projects.get(projectNumber).getId();
        @Nullable final ProjectDTO project = projectEndpoint.findOneProjectByUserId(currentSessionService.getSessionDTO(), projectId);
        if (project == null) {
            throw new Exception("project doesn't exist");
        }
        printProject(project);
        System.out.println("[OK]");
    }
}
