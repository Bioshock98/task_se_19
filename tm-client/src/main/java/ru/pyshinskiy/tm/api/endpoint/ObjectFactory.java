
package ru.pyshinskiy.tm.api.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.pyshinskiy.tm.api.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.api.tm.pyshinskiy.ru/", "Exception");
    private final static QName _CreateSession_QNAME = new QName("http://endpoint.api.tm.pyshinskiy.ru/", "createSession");
    private final static QName _CreateSessionResponse_QNAME = new QName("http://endpoint.api.tm.pyshinskiy.ru/", "createSessionResponse");
    private final static QName _IsSessionExist_QNAME = new QName("http://endpoint.api.tm.pyshinskiy.ru/", "isSessionExist");
    private final static QName _IsSessionExistResponse_QNAME = new QName("http://endpoint.api.tm.pyshinskiy.ru/", "isSessionExistResponse");
    private final static QName _RemoveAllSessions_QNAME = new QName("http://endpoint.api.tm.pyshinskiy.ru/", "removeAllSessions");
    private final static QName _RemoveAllSessionsByUserId_QNAME = new QName("http://endpoint.api.tm.pyshinskiy.ru/", "removeAllSessionsByUserId");
    private final static QName _RemoveAllSessionsByUserIdResponse_QNAME = new QName("http://endpoint.api.tm.pyshinskiy.ru/", "removeAllSessionsByUserIdResponse");
    private final static QName _RemoveAllSessionsResponse_QNAME = new QName("http://endpoint.api.tm.pyshinskiy.ru/", "removeAllSessionsResponse");
    private final static QName _RemoveSession_QNAME = new QName("http://endpoint.api.tm.pyshinskiy.ru/", "removeSession");
    private final static QName _RemoveSessionResponse_QNAME = new QName("http://endpoint.api.tm.pyshinskiy.ru/", "removeSessionResponse");
    private final static QName _UpdateSession_QNAME = new QName("http://endpoint.api.tm.pyshinskiy.ru/", "updateSession");
    private final static QName _UpdateSessionResponse_QNAME = new QName("http://endpoint.api.tm.pyshinskiy.ru/", "updateSessionResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.pyshinskiy.tm.api.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link CreateSession }
     * 
     */
    public CreateSession createCreateSession() {
        return new CreateSession();
    }

    /**
     * Create an instance of {@link CreateSessionResponse }
     * 
     */
    public CreateSessionResponse createCreateSessionResponse() {
        return new CreateSessionResponse();
    }

    /**
     * Create an instance of {@link IsSessionExist }
     * 
     */
    public IsSessionExist createIsSessionExist() {
        return new IsSessionExist();
    }

    /**
     * Create an instance of {@link IsSessionExistResponse }
     * 
     */
    public IsSessionExistResponse createIsSessionExistResponse() {
        return new IsSessionExistResponse();
    }

    /**
     * Create an instance of {@link RemoveAllSessions }
     * 
     */
    public RemoveAllSessions createRemoveAllSessions() {
        return new RemoveAllSessions();
    }

    /**
     * Create an instance of {@link RemoveAllSessionsByUserId }
     * 
     */
    public RemoveAllSessionsByUserId createRemoveAllSessionsByUserId() {
        return new RemoveAllSessionsByUserId();
    }

    /**
     * Create an instance of {@link RemoveAllSessionsByUserIdResponse }
     * 
     */
    public RemoveAllSessionsByUserIdResponse createRemoveAllSessionsByUserIdResponse() {
        return new RemoveAllSessionsByUserIdResponse();
    }

    /**
     * Create an instance of {@link RemoveAllSessionsResponse }
     * 
     */
    public RemoveAllSessionsResponse createRemoveAllSessionsResponse() {
        return new RemoveAllSessionsResponse();
    }

    /**
     * Create an instance of {@link RemoveSession }
     * 
     */
    public RemoveSession createRemoveSession() {
        return new RemoveSession();
    }

    /**
     * Create an instance of {@link RemoveSessionResponse }
     * 
     */
    public RemoveSessionResponse createRemoveSessionResponse() {
        return new RemoveSessionResponse();
    }

    /**
     * Create an instance of {@link UpdateSession }
     * 
     */
    public UpdateSession createUpdateSession() {
        return new UpdateSession();
    }

    /**
     * Create an instance of {@link UpdateSessionResponse }
     * 
     */
    public UpdateSessionResponse createUpdateSessionResponse() {
        return new UpdateSessionResponse();
    }

    /**
     * Create an instance of {@link SessionDTO }
     * 
     */
    public SessionDTO createSessionDTO() {
        return new SessionDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.pyshinskiy.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.pyshinskiy.ru/", name = "createSession")
    public JAXBElement<CreateSession> createCreateSession(CreateSession value) {
        return new JAXBElement<CreateSession>(_CreateSession_QNAME, CreateSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.pyshinskiy.ru/", name = "createSessionResponse")
    public JAXBElement<CreateSessionResponse> createCreateSessionResponse(CreateSessionResponse value) {
        return new JAXBElement<CreateSessionResponse>(_CreateSessionResponse_QNAME, CreateSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsSessionExist }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.pyshinskiy.ru/", name = "isSessionExist")
    public JAXBElement<IsSessionExist> createIsSessionExist(IsSessionExist value) {
        return new JAXBElement<IsSessionExist>(_IsSessionExist_QNAME, IsSessionExist.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsSessionExistResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.pyshinskiy.ru/", name = "isSessionExistResponse")
    public JAXBElement<IsSessionExistResponse> createIsSessionExistResponse(IsSessionExistResponse value) {
        return new JAXBElement<IsSessionExistResponse>(_IsSessionExistResponse_QNAME, IsSessionExistResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveAllSessions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.pyshinskiy.ru/", name = "removeAllSessions")
    public JAXBElement<RemoveAllSessions> createRemoveAllSessions(RemoveAllSessions value) {
        return new JAXBElement<RemoveAllSessions>(_RemoveAllSessions_QNAME, RemoveAllSessions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveAllSessionsByUserId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.pyshinskiy.ru/", name = "removeAllSessionsByUserId")
    public JAXBElement<RemoveAllSessionsByUserId> createRemoveAllSessionsByUserId(RemoveAllSessionsByUserId value) {
        return new JAXBElement<RemoveAllSessionsByUserId>(_RemoveAllSessionsByUserId_QNAME, RemoveAllSessionsByUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveAllSessionsByUserIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.pyshinskiy.ru/", name = "removeAllSessionsByUserIdResponse")
    public JAXBElement<RemoveAllSessionsByUserIdResponse> createRemoveAllSessionsByUserIdResponse(RemoveAllSessionsByUserIdResponse value) {
        return new JAXBElement<RemoveAllSessionsByUserIdResponse>(_RemoveAllSessionsByUserIdResponse_QNAME, RemoveAllSessionsByUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveAllSessionsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.pyshinskiy.ru/", name = "removeAllSessionsResponse")
    public JAXBElement<RemoveAllSessionsResponse> createRemoveAllSessionsResponse(RemoveAllSessionsResponse value) {
        return new JAXBElement<RemoveAllSessionsResponse>(_RemoveAllSessionsResponse_QNAME, RemoveAllSessionsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.pyshinskiy.ru/", name = "removeSession")
    public JAXBElement<RemoveSession> createRemoveSession(RemoveSession value) {
        return new JAXBElement<RemoveSession>(_RemoveSession_QNAME, RemoveSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.pyshinskiy.ru/", name = "removeSessionResponse")
    public JAXBElement<RemoveSessionResponse> createRemoveSessionResponse(RemoveSessionResponse value) {
        return new JAXBElement<RemoveSessionResponse>(_RemoveSessionResponse_QNAME, RemoveSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.pyshinskiy.ru/", name = "updateSession")
    public JAXBElement<UpdateSession> createUpdateSession(UpdateSession value) {
        return new JAXBElement<UpdateSession>(_UpdateSession_QNAME, UpdateSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.pyshinskiy.ru/", name = "updateSessionResponse")
    public JAXBElement<UpdateSessionResponse> createUpdateSessionResponse(UpdateSessionResponse value) {
        return new JAXBElement<UpdateSessionResponse>(_UpdateSessionResponse_QNAME, UpdateSessionResponse.class, null, value);
    }

}
