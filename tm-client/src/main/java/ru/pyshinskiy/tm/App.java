package ru.pyshinskiy.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.BeanFactoryAnnotationUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.pyshinskiy.tm.api.endpoint.ISessionEndpoint;
import ru.pyshinskiy.tm.api.endpoint.SessionDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.command.system.HelpCommand;
import ru.pyshinskiy.tm.config.ApplicationConfig;
import ru.pyshinskiy.tm.service.CurrentSessionService;
import ru.pyshinskiy.tm.service.TerminalService;

import java.util.Collection;
import java.util.stream.Collectors;

public final class App {

    @Nullable
    private static String allCommandBeansInStringFormat;

    public static void main(String[] args) throws Exception {
        @NotNull final ApplicationContext appContext = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        @NotNull final Collection<AbstractCommand> commands = appContext.getBeansOfType(AbstractCommand.class).values();
        appContext.getBean("helpCommand", HelpCommand.class).setAbstractCommands(commands);
        allCommandBeansInStringFormat = commands.stream()
                .filter(e -> e.getClass().isAnnotationPresent(Qualifier.class))
                .map(e -> e.getClass().getAnnotation(Qualifier.class).value())
                .collect(Collectors.joining());
        System.out.println("***WELCOME TO TASK MANAGER***");
        @NotNull final TerminalService terminalService = appContext.getBean("terminalService", TerminalService.class);
        @Nullable String command;
        while(true) {
            command = terminalService.nextLine();
            try {
                execute(appContext, command);
            }
            catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    private static void execute(@NotNull final ApplicationContext appContext, @Nullable final String command) throws Exception {
        @NotNull final CurrentSessionService currentSessionService = appContext.getBean("currentSessionService", CurrentSessionService.class);
        @NotNull final ISessionEndpoint sessionEndpoint = appContext.getBean("sessionEndpoint", ISessionEndpoint.class);
        if(command == null || command.isEmpty()) return;
        if("exit".equals(command)) {
            @Nullable SessionDTO currentSessionDTO = currentSessionService.getSessionDTO();
            if(currentSessionDTO != null) sessionEndpoint.removeSession(currentSessionDTO.getUserId(), currentSessionDTO.getId());
            System.exit(0);
        }
        if(!allCommandBeansInStringFormat.contains(command)) {
            System.out.println("UNKNOW COMMAND");
            return;
        }
        @NotNull final AbstractCommand abstractCommand = BeanFactoryAnnotationUtils.qualifiedBeanOfType(
                appContext.getAutowireCapableBeanFactory(),
                AbstractCommand.class,
                command);
        if(!abstractCommand.isAllowed()) {
            System.out.println("!UNAVAILABLE COMMAND!");
            return;
        }
        abstractCommand.execute();
    }
}
