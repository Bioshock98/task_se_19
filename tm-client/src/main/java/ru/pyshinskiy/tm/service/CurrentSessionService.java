package ru.pyshinskiy.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.SessionDTO;

@Component
@Getter
@Setter
public class CurrentSessionService {

    private SessionDTO sessionDTO;
}
