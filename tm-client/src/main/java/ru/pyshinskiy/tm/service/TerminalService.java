package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;

@Component
public final class TerminalService {

    @Autowired
    @NotNull
    private BufferedReader bufferedReader;

    @NotNull
    public String nextLine() throws Exception {
        return bufferedReader.readLine();
    }
}
