package ru.pyshinskiy.tm.daemon;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.Exception_Exception;
import ru.pyshinskiy.tm.api.endpoint.ISessionEndpoint;
import ru.pyshinskiy.tm.api.endpoint.SessionDTO;
import ru.pyshinskiy.tm.service.CurrentSessionService;

@Component
public final class SessionUpdater extends Thread {

    @Autowired
    @Nullable
    private ISessionEndpoint sessionEndpoint;

    @Autowired
    @Nullable
    private CurrentSessionService currentSessionService;

    @Override
    public void run() {
        while(true) {
            if(currentSessionService.getSessionDTO() != null) {
                try {
                    Thread.sleep(150000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                @Nullable SessionDTO updatedSession = null;
                try {
                    updatedSession = sessionEndpoint.updateSession(currentSessionService.getSessionDTO());
                    currentSessionService.setSessionDTO(updatedSession);
                    System.out.println("[Session update]");
                }
                catch (Exception_Exception e) {
                    System.out.println("Updating session is failed");
                }
            }
        }
    }
}
