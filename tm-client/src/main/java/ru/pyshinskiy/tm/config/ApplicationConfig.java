package ru.pyshinskiy.tm.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ISessionEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.IUserEndpoint;
import ru.pyshinskiy.tm.endpoint.ProjectEndpointService;
import ru.pyshinskiy.tm.endpoint.SessionEndpointService;
import ru.pyshinskiy.tm.endpoint.TaskEndpointService;
import ru.pyshinskiy.tm.endpoint.UserEndpointService;

import java.io.BufferedReader;
import java.io.InputStreamReader;

@Configuration
@ComponentScan(basePackages = "ru.pyshinskiy.tm")
public class ApplicationConfig {

    @Bean
    public BufferedReader bufferedReader() {
        return new BufferedReader(new InputStreamReader(System.in));
    }

    @Bean
    @Qualifier("projectEndpoint")
    public IProjectEndpoint projectEndpoint() {
        return new ProjectEndpointService().getProjectEndpointPort();
    }

    @Bean
    @Qualifier("taskEndpoint")
    public ITaskEndpoint taskEndpoint() {
        return new TaskEndpointService().getTaskEndpointPort();
    }

    @Bean
    @Qualifier("sessionEndpoint")
    public ISessionEndpoint sessionEndpoint() {
        return new SessionEndpointService().getSessionEndpointPort();
    }

    @Bean
    @Qualifier("userEndpoint")
    public IUserEndpoint userEndpoint() {
        return new UserEndpointService().getUserEndpointPort();
    }
}
