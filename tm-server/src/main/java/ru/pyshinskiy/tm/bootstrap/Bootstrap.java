package ru.pyshinskiy.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.daemon.SessionCleaner;
import ru.pyshinskiy.tm.endpoint.AbstractEndpoint;

import javax.xml.ws.Endpoint;
import java.util.List;

@Component
@PropertySource("classpath:endpoint.properties")
public final class Bootstrap {

    @Nullable
    @Autowired
    private List<AbstractEndpoint> endpoints;

    @NotNull
    @Value("${endpoint.host}")
    private String host;

    public void start(@NotNull final SessionCleaner sessionCleaner) {
        endpoints.stream().forEach(e -> Endpoint.publish(host + e.getClass().getSimpleName() + "?wsdl", e));
        sessionCleaner.setDaemon(true);
        sessionCleaner.start();
    }
}
