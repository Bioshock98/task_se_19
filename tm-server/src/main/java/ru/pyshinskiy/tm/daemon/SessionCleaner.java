package ru.pyshinskiy.tm.daemon;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.session.ISessionService;
import ru.pyshinskiy.tm.entity.Session;

import java.util.Date;

import static ru.pyshinskiy.tm.constant.AppConst.SESSION_LIFE_TIME;

@Component
public class SessionCleaner extends Thread {

    @Autowired
    @Nullable
    private ISessionService sessionService;

    @Override
    public void run() {
        while(true) {
            try {
                for(@NotNull final Session session : sessionService.findAll()) {
                    if(new Date().getTime() - session.getTimestamp().getTime() > SESSION_LIFE_TIME) sessionService.remove(session);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(60000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
