package ru.pyshinskiy.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.dto.ProjectDTO;
import ru.pyshinskiy.tm.dto.SessionDTO;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@Component
@NoArgsConstructor
@WebService(endpointInterface = "ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @WebMethod
    @Nullable
    @Override
    public ProjectDTO findOneProject(@Nullable final SessionDTO sessionDTO, @Nullable final String id) throws Exception {
        validateSession(sessionDTO);
        if(!sessionDTO.getRole().equals(Role.ADMINISTRATOR)) throw new Exception("ACCESS DENIED");
        return Project.toProjectDTO(serviceLocator.getProjectService().findOne(id));
    }

    @WebMethod
    @NotNull
    @Override
    public List<ProjectDTO> findAllProjects(@Nullable final SessionDTO sessionDTO) throws Exception {
        validateSession(sessionDTO);
        if(!sessionDTO.getRole().equals(Role.ADMINISTRATOR)) throw new Exception("ACCESS DENIED");
        return Project.toProjectsDTO(serviceLocator.getProjectService().findAll());
    }

    @WebMethod
    @Override
    public void persistProject(@Nullable final SessionDTO sessionDTO, @Nullable final ProjectDTO projectDTO) throws Exception {
        validateSession(sessionDTO);
        serviceLocator.getProjectService().persist(ProjectDTO.toProject(serviceLocator, projectDTO));
    }

    @WebMethod
    @Override
    public void mergeProject(@Nullable final SessionDTO sessionDTO, @Nullable final ProjectDTO projectDTO) throws Exception {
        validateSession(sessionDTO);
        if(!sessionDTO.getUserId().equals(projectDTO.getUserId())) throw new Exception("ACCESS DENIED");
        serviceLocator.getProjectService().merge(ProjectDTO.toProject(serviceLocator, projectDTO));
    }

    @WebMethod
    @Override
    public void removeProject(@Nullable final SessionDTO sessionDTO, @Nullable final String id) throws Exception {
        validateSession(sessionDTO);
        if(!sessionDTO.getRole().equals(Role.ADMINISTRATOR)) throw new Exception("ACCESS DENIED");
        serviceLocator.getProjectService().remove(serviceLocator.getProjectService().findOne(id));
    }

    @WebMethod
    @Override
    public void removeAllProjects(@Nullable final SessionDTO sessionDTO) throws Exception {
        validateSession(sessionDTO);
        if(!sessionDTO.getRole().equals(Role.ADMINISTRATOR)) throw new Exception("ACCESS DENIED");
        serviceLocator.getProjectService().removeAll();
    }

    @WebMethod
    @Override
    @Nullable
    public ProjectDTO findOneProjectByUserId(@Nullable final SessionDTO sessionDTO, @Nullable final String id) throws Exception {
        @NotNull final Session session = SessionDTO.toSession(serviceLocator, sessionDTO);
        validateSession(sessionDTO);
        return Project.toProjectDTO(serviceLocator.getProjectService().findOneByUserId(session.getUser(), id));
    }

    @WebMethod
    @Override
    @NotNull
    public List<ProjectDTO> findAllProjectsByUserId(@Nullable final SessionDTO sessionDTO) throws Exception{
        @NotNull final Session session = SessionDTO.toSession(serviceLocator, sessionDTO);
        validateSession(sessionDTO);
        return Project.toProjectsDTO(serviceLocator.getProjectService().findAllByUserId(session.getUser()));
    }

    @WebMethod
    @Override
    public void removeProjectByUserId(@Nullable final SessionDTO sessionDTO, @Nullable final String id) throws Exception {
        validateSession(sessionDTO);
        serviceLocator.getProjectService().removeByUserId(serviceLocator.getUserService().findOne(sessionDTO.getUserId()), id);
    }

    @WebMethod
    @Override
    public void removeAllProjectsByUserId(@Nullable final SessionDTO sessionDTO) throws Exception {
        @NotNull final Session session = SessionDTO.toSession(serviceLocator, sessionDTO);
        validateSession(sessionDTO);
        serviceLocator.getProjectService().removeAll(session.getUser());
    }

    @WebMethod
    @Override
    @NotNull
    public List<ProjectDTO> findProjectByName(@Nullable final SessionDTO sessionDTO, @Nullable final String name) throws Exception {
        @NotNull final Session session = SessionDTO.toSession(serviceLocator, sessionDTO);
        validateSession(sessionDTO);
        return Project.toProjectsDTO(serviceLocator.getProjectService().findByName(session.getUser(), name));
    }

    @WebMethod
    @Override
    @NotNull
    public List<ProjectDTO> findProjectByDescription(@Nullable final SessionDTO sessionDTO, @Nullable final String description) throws Exception {
        @NotNull final Session session = SessionDTO.toSession(serviceLocator, sessionDTO);
        validateSession(sessionDTO);
        return Project.toProjectsDTO(serviceLocator.getProjectService().findByDescription(session.getUser(), description));
    }

    @WebMethod
    @Override
    @NotNull
    public List<ProjectDTO> sortProjectsByCreateTime(@Nullable final SessionDTO sessionDTO, @Nullable final String userId, final int direction) throws Exception {
        validateSession(sessionDTO);
        return Project.toProjectsDTO(serviceLocator.getProjectService().sortByCreateTime(serviceLocator.getUserService().findOne(userId)));
    }

    @WebMethod
    @Override
    @NotNull
    public List<ProjectDTO> sortProjectsByStartDate(@Nullable final SessionDTO sessionDTO, @Nullable final String userId, final int direction) throws Exception {
        validateSession(sessionDTO);
        return Project.toProjectsDTO(serviceLocator.getProjectService().sortByStartDate(serviceLocator.getUserService().findOne(userId)));
    }

    @WebMethod
    @Override
    @NotNull
    public List<ProjectDTO> sortProjectsByFinishDate(@Nullable final SessionDTO sessionDTO, @Nullable final String userId, final int direction) throws Exception {
        validateSession(sessionDTO);
        return Project.toProjectsDTO(serviceLocator.getProjectService().sortByFinishDate(serviceLocator.getUserService().findOne(userId)));
    }

    @WebMethod
    @Override
    @NotNull
    public List<ProjectDTO> sortProjectsByStatus(@Nullable final SessionDTO sessionDTO, @Nullable final String userId, final int direction) throws Exception {
        validateSession(sessionDTO);
        return Project.toProjectsDTO(serviceLocator.getProjectService().sortByStatus(serviceLocator.getUserService().findOne(userId)));
    }
}
