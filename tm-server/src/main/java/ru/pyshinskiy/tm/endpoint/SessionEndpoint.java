package ru.pyshinskiy.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.ISessionEndpoint;
import ru.pyshinskiy.tm.dto.SessionDTO;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.enumerated.Role;
import ru.pyshinskiy.tm.util.security.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;

import static ru.pyshinskiy.tm.constant.AppConst.CICLE;
import static ru.pyshinskiy.tm.constant.AppConst.SALT;

@Component
@NoArgsConstructor
@WebService(endpointInterface = "ru.pyshinskiy.tm.api.endpoint.ISessionEndpoint")
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @WebMethod
    @Override
    public boolean isSessionExist(@Nullable final String id) {
        return serviceLocator.getSessionService().isSessionExist(id);
    }

    @WebMethod
    @Override
    @Nullable
    public SessionDTO createSession(@Nullable final String login, @Nullable final String password) throws Exception {
        if(login == null || password == null) return null;
        @Nullable final User user = serviceLocator.getUserService().getUserByLogin(login);
        if(user == null) return null;
        if(!user.getPasswordHash().equals(SignatureUtil.sign(password, SALT, CICLE))) return null;
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setUserId(user.getId());
        sessionDTO.setRole(user.getRole());
        @NotNull final Session session = SessionDTO.toSession(serviceLocator, sessionDTO);
        session.setSignature(SignatureUtil.sign(sessionDTO, SALT, CICLE));
        serviceLocator.getSessionService().persist(session);
        return sessionDTO;
    }

    @WebMethod
    @Override
    @NotNull
    public SessionDTO updateSession(@Nullable final SessionDTO sessionDTO) throws Exception {
        if(sessionDTO == null) return null;
        @Nullable User user = serviceLocator.getUserService().findOne(sessionDTO.getUserId());
        @Nullable final Session oldSession = serviceLocator.getSessionService().findOneByUserId(user, sessionDTO.getId());
        @NotNull final SessionDTO newSessionDTO = new SessionDTO();
        newSessionDTO.setId(oldSession.getId());
        newSessionDTO.setUserId(oldSession.getUser().getId());
        newSessionDTO.setRole(oldSession.getRole());
        @NotNull final Session newSession = SessionDTO.toSession(serviceLocator, newSessionDTO);
        newSession.setSignature(SignatureUtil.sign(newSessionDTO, SALT, CICLE));
        serviceLocator.getSessionService().merge(newSession);
        return newSessionDTO;
    }

    @WebMethod
    @Override
    public void removeSession(@Nullable final String userId, @Nullable final String id) throws Exception {
        if(userId == null || id == null) return;
        serviceLocator.getSessionService().removeByUserId(serviceLocator.getUserService().findOne(userId), id);
    }

    @WebMethod
    @Override
    public void removeAllSessions(@Nullable final SessionDTO sessionDTO) throws Exception {
        validateSession(sessionDTO);
        if(!sessionDTO.getRole().equals(Role.ADMINISTRATOR)) throw new Exception("ACCESS DENIED");
        serviceLocator.getSessionService().removeAll();
    }

    @WebMethod
    @Override
    public void removeAllSessionsByUserId(@Nullable final String userId) throws Exception {
        if(userId == null) return;
        serviceLocator.getSessionService().removeAllByUserId(serviceLocator.getUserService().findOne(userId));
    }
}
