package ru.pyshinskiy.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.dto.SessionDTO;
import ru.pyshinskiy.tm.dto.TaskDTO;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@Component
@NoArgsConstructor
@WebService(endpointInterface = "ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @WebMethod
    @Nullable
    @Override
    public TaskDTO findOneTask(@Nullable final SessionDTO sessionDTO, @Nullable final String id) throws Exception {
        validateSession(sessionDTO);
        return Task.toTaskDTO(serviceLocator.getTaskService().findOne(id));
    }

    @WebMethod
    @NotNull
    @Override
    public List<TaskDTO> findAllTasks(@Nullable final SessionDTO sessionDTO) throws Exception {
        validateSession(sessionDTO);
        return Task.toTasksDTO(serviceLocator.getTaskService().findAll());
    }

    @WebMethod
    @Override
    public void persistTask(@Nullable final SessionDTO sessionDTO, @Nullable final TaskDTO taskDTO) throws Exception {
        validateSession(sessionDTO);
        serviceLocator.getTaskService().persist(TaskDTO.toTask(serviceLocator, taskDTO));
    }

    @WebMethod
    @Override
    public void mergeTask(@Nullable final SessionDTO sessionDTO, @Nullable final TaskDTO taskDTO) throws Exception {
        validateSession(sessionDTO);
        serviceLocator.getTaskService().merge(TaskDTO.toTask(serviceLocator, taskDTO));
    }

    @WebMethod
    @Override
    public void removeTask(@Nullable final SessionDTO sessionDTO, @Nullable final String id) throws Exception {
        validateSession(sessionDTO);
        serviceLocator.getTaskService().remove(serviceLocator.getTaskService().findOne(id));
    }

    @WebMethod
    @Override
    public void removeAllTasks(@Nullable final SessionDTO sessionDTO) throws Exception {
        validateSession(sessionDTO);
        serviceLocator.getTaskService().removeAll();
    }

    @WebMethod
    @Override
    @Nullable
    public TaskDTO findOneTaskByUserId(@Nullable final SessionDTO sessionDTO, @Nullable final String id) throws Exception {
        @NotNull final Session session = SessionDTO.toSession(serviceLocator, sessionDTO);
        validateSession(sessionDTO);
        return Task.toTaskDTO(serviceLocator.getTaskService().findOneByUserId(session.getUser(), id));
    }

    @WebMethod
    @Override
    @NotNull
    public List<TaskDTO> findAllTasksByUserId(@Nullable final SessionDTO sessionDTO) throws Exception{
        @NotNull final Session session = SessionDTO.toSession(serviceLocator, sessionDTO);
        validateSession(sessionDTO);
        return Task.toTasksDTO(serviceLocator.getTaskService().findAllByUserId(session.getUser()));
    }

    @WebMethod
    @Override
    public void removeTaskByUserId(@Nullable final SessionDTO sessionDTO, @Nullable final String id) throws Exception {
        @NotNull final Session session = SessionDTO.toSession(serviceLocator, sessionDTO);
        validateSession(sessionDTO);
        serviceLocator.getTaskService().removeByUserId(session.getUser(), id);
    }

    @WebMethod
    @Override
    public void removeAllTasksByUserId(@Nullable final SessionDTO sessionDTO) throws Exception {
        @NotNull final Session session = SessionDTO.toSession(serviceLocator, sessionDTO);
        validateSession(sessionDTO);
        serviceLocator.getTaskService().removeAll(session.getUser());
    }

    @WebMethod
    @Override
    @NotNull
    public List<TaskDTO> findTasksByName(@Nullable final SessionDTO sessionDTO, @Nullable final String name) throws Exception {
        @NotNull final Session session = SessionDTO.toSession(serviceLocator, sessionDTO);
        validateSession(sessionDTO);
        return Task.toTasksDTO(serviceLocator.getTaskService().findByName(session.getUser(), name));
    }

    @WebMethod
    @Override
    @NotNull
    public List<TaskDTO> findTasksByDescription(@Nullable final SessionDTO sessionDTO, @Nullable final String description) throws Exception {
        @NotNull final Session session = SessionDTO.toSession(serviceLocator, sessionDTO);
        validateSession(sessionDTO);
        return Task.toTasksDTO(serviceLocator.getTaskService().findByDescription(session.getUser(), description));
    }

    @WebMethod
    @Override
    @NotNull
    public List<TaskDTO> sortTasksByCreateTime(@Nullable final SessionDTO sessionDTO, @Nullable final String userId, final int direction) throws Exception {
        validateSession(sessionDTO);
        return Task.toTasksDTO(serviceLocator.getTaskService().sortByCreateTime(serviceLocator.getUserService().findOne(userId)));
    }

    @WebMethod
    @Override
    @NotNull
    public List<TaskDTO> sortTasksByStartDate(@Nullable final SessionDTO sessionDTO, @Nullable final String userId, final int direction) throws Exception {
        validateSession(sessionDTO);
        return Task.toTasksDTO(serviceLocator.getTaskService().sortByStartDate(serviceLocator.getUserService().findOne(userId)));
    }

    @WebMethod
    @Override
    @NotNull
    public List<TaskDTO> sortTasksByFinishDate(@Nullable final SessionDTO sessionDTO, @Nullable final String userId, final int direction) throws Exception {
        validateSession(sessionDTO);
        return Task.toTasksDTO(serviceLocator.getTaskService().sortByFinishDate(serviceLocator.getUserService().findOne(userId)));
    }

    @WebMethod
    @Override
    @NotNull
    public List<TaskDTO> sortTasksByStatus(@Nullable final SessionDTO sessionDTO, @Nullable final String userId, final int direction) throws Exception {
        validateSession(sessionDTO);
        return Task.toTasksDTO(serviceLocator.getTaskService().sortByStatus(serviceLocator.getUserService().findOne(userId)));
    }

    @WebMethod
    @Override
    @NotNull
    public List<TaskDTO> findTasksByProjectId(@Nullable final SessionDTO sessionDTO, @Nullable final String projectId) throws Exception {
        @NotNull final Session session = SessionDTO.toSession(serviceLocator, sessionDTO);
        validateSession(sessionDTO);
        return Task.toTasksDTO(serviceLocator.getTaskService().findAllByProjectId(session.getUser(), serviceLocator.getProjectService().findOne(projectId)));
    }
}
