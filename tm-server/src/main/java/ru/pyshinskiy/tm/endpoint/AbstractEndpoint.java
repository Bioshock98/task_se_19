package ru.pyshinskiy.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.pyshinskiy.tm.dto.SessionDTO;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.service.ServiceLocator;

import java.util.Date;

import static ru.pyshinskiy.tm.constant.AppConst.*;
import static ru.pyshinskiy.tm.util.security.SignatureUtil.sign;

@NoArgsConstructor
public abstract class AbstractEndpoint {

    @Autowired
    @Nullable
    protected ServiceLocator serviceLocator;

    protected void validateSession(@Nullable final SessionDTO userSession) throws Exception {
        if(userSession == null) throw new Exception("method is unavailable for unauthorized users");
        @NotNull final User user = serviceLocator.getUserService().findOne(userSession.getUserId());
        @Nullable final Session session = serviceLocator.getSessionService().findOneByUserId(user, userSession.getId());
        if(session == null) throw new Exception("Session is doesn't exist");
        if(!session.getSignature().equals(sign(userSession, SALT, CICLE))) {
            throw new Exception("Session is invalid");
        }
        if(new Date().getTime() - userSession.getTimestamp().getTime() > SESSION_LIFE_TIME) {
            serviceLocator.getSessionService().removeByUserId(user, userSession.getId());
            throw new Exception("Session time out\n you need to log in again");
        }
    }
}
