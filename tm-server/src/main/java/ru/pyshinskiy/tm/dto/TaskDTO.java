package ru.pyshinskiy.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.service.ServiceLocator;

@Getter
@Setter
@NoArgsConstructor
public final class TaskDTO extends AbstractWBSDTO {

    @Nullable
    private String projectId;


    @NotNull
    public static Task toTask(@NotNull final ServiceLocator serviceLocator, @NotNull final TaskDTO taskDTO) throws Exception {
        @NotNull final Task task = new Task();
        task.setId(taskDTO.getId());
        task.setUser(serviceLocator.getUserService().findOne(taskDTO.getUserId()));
        if(taskDTO.getProjectId() == null || taskDTO.getProjectId().isEmpty()) task.setProject(null);
        else task.setProject(serviceLocator.getProjectService().findOne(taskDTO.getProjectId()));
        task.setCreateTime(taskDTO.getCreateTime());
        task.setName(taskDTO.getName());
        task.setDescription(taskDTO.getDescription());
        task.setStartDate(taskDTO.getStartDate());
        task.setFinishDate(taskDTO.getFinishDate());
        task.setStatus(taskDTO.getStatus());
        return task;
    }
}
