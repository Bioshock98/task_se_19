package ru.pyshinskiy.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.service.ServiceLocator;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectDTO extends AbstractWBSDTO {

    @NotNull
    public static Project toProject(@NotNull final ServiceLocator serviceLocator, @NotNull final ProjectDTO projectDTO) throws Exception {
        @NotNull final Project project = new Project();
        project.setId(projectDTO.getId());
        project.setUser(serviceLocator.getUserService().findOne(projectDTO.getUserId()));
        project.setCreateTime(projectDTO.getCreateTime());
        project.setName(projectDTO.getName());
        project.setDescription(projectDTO.getDescription());
        project.setStartDate(projectDTO.getStartDate());
        project.setFinishDate(projectDTO.getFinishDate());
        project.setStatus(projectDTO.getStatus());
        @NotNull final User user = serviceLocator.getUserService().findOne(projectDTO.getUserId());
        project.setTasks(serviceLocator.getTaskService().findAllByProjectId(user, project));
        return project;
    }
}
