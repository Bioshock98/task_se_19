package ru.pyshinskiy.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.enumerated.Role;
import ru.pyshinskiy.tm.service.ServiceLocator;

@Getter
@Setter
@NoArgsConstructor
public final class UserDTO extends AbstractEntityDTO {

    @NotNull
    private String login;


    @NotNull
    private String passwordHash;

    @NotNull
    private Role role;

    @NotNull
    public static User toUser(@NotNull final ServiceLocator serviceLocator, @NotNull final UserDTO userDTO) throws Exception{
        @NotNull final User user = new User();
        user.setId(userDTO.getId());
        user.setLogin(userDTO.getLogin());
        user.setPasswordHash(userDTO.getPasswordHash());
        user.setRole(userDTO.getRole());
        user.setProjects(serviceLocator.getProjectService().findAllByUserId(user));
        user.setTasks(serviceLocator.getTaskService().findAllByUserId(user));
        return user;
    }
}
