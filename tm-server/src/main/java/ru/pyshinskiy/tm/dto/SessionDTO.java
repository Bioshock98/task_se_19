package ru.pyshinskiy.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.enumerated.Role;
import ru.pyshinskiy.tm.service.ServiceLocator;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class SessionDTO extends AbstractEntityDTO {


    @NotNull
    String userId;

    @NotNull
    Role role;

    @NotNull Date timestamp = new Date(System.currentTimeMillis());

    @NotNull
    public static Session toSession(@NotNull final ServiceLocator serviceLocator, @NotNull final SessionDTO sessionDTO) throws Exception {
        @NotNull final Session session = new Session();
        session.setId(sessionDTO.getId());
        session.setUser(serviceLocator.getUserService().findOne(sessionDTO.getUserId()));
        session.setRole(sessionDTO.getRole());
        session.setTimestamp(sessionDTO.getTimestamp());
        return session;
    }
}
