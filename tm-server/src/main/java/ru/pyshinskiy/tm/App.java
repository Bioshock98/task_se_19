package ru.pyshinskiy.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.pyshinskiy.tm.bootstrap.Bootstrap;
import ru.pyshinskiy.tm.config.ApplicationConfig;
import ru.pyshinskiy.tm.daemon.SessionCleaner;

public final class App {

    public static void main(String[] args) {
        @NotNull final ApplicationContext appContext = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        @NotNull final Bootstrap bootstrap = appContext.getBean("bootstrap", Bootstrap.class);
        bootstrap.start(appContext.getBean("sessionCleaner", SessionCleaner.class));
    }
}
