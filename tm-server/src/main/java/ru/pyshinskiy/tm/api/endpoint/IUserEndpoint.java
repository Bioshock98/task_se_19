package ru.pyshinskiy.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.dto.SessionDTO;
import ru.pyshinskiy.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @WebMethod
    @Nullable
    UserDTO findOneUser(@Nullable final SessionDTO sessionDTO, @Nullable final String id) throws Exception;

    @WebMethod
    @NotNull
    List<UserDTO> findAllUsers(@Nullable final SessionDTO sessionDTO) throws Exception;

    @WebMethod
    void persistUser(@NotNull final UserDTO userDTO) throws Exception;

    @WebMethod
    void mergeUser(@Nullable final SessionDTO sessionDTO, @Nullable final UserDTO userDTO) throws Exception;

    @WebMethod
    void removeUser(@Nullable final SessionDTO sessionDTO, @Nullable final String id) throws Exception;

    @WebMethod
    void removeAllUsers(@Nullable final SessionDTO sessionDTO) throws Exception;

    @WebMethod
    boolean isUserExistByLogin(@Nullable final SessionDTO sessionDTO, @Nullable final String login) throws Exception;
}
