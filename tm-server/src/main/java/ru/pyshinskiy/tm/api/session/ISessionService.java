package ru.pyshinskiy.tm.api.session;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.service.IService;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.entity.User;

import java.util.List;

public interface ISessionService extends IService<Session> {

    boolean isSessionExist(@Nullable final String id);

    @Nullable
    Session findOneByUserId(@NotNull final User user, @NotNull final String id) throws Exception;

    List<Session> findAllByUserId(@NotNull final User user) throws Exception;

    void removeByUserId(@NotNull final User user, @NotNull final String id) throws Exception;

    void removeAllByUserId(@Nullable final User user) throws Exception;
}
