package ru.pyshinskiy.tm.api.user;

import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.service.IService;
import ru.pyshinskiy.tm.entity.User;

public interface IUserService extends IService<User> {

    @Nullable
    User getUserByLogin(@Nullable final String login) throws Exception;

    boolean isUserExistByLogin(@Nullable final String login) throws Exception;
}
