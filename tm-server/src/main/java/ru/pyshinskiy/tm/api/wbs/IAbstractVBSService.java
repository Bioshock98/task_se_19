package ru.pyshinskiy.tm.api.wbs;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.service.IService;
import ru.pyshinskiy.tm.entity.User;

import java.util.List;

public interface IAbstractVBSService<T> extends IService<T> {

    @NotNull
    List<T> findAllByUserId(@Nullable final User user) throws Exception;

    @Nullable
    T findOneByUserId(@Nullable final User user, @Nullable final String id) throws Exception;

    @Nullable
    List<T> findByName(@Nullable final User user, @Nullable final String name) throws Exception;

    @Nullable
    List<T> findByDescription(@Nullable final User user, @Nullable final String description) throws Exception;

    void removeByUserId(@Nullable final User user, @Nullable final String id) throws Exception;

    void removeAll(@Nullable final User user) throws Exception;

    @NotNull
    List<T> sortByCreateTime(@Nullable final User user) throws Exception;

    @NotNull
    List<T> sortByStartDate(@Nullable final User user) throws Exception;

    @NotNull
    List<T> sortByFinishDate(@Nullable final User user) throws Exception;

    @NotNull
    List<T> sortByStatus(@Nullable final User user) throws Exception;
}
