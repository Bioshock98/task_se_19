package ru.pyshinskiy.tm.api.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.wbs.IAbstractVBSService;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.entity.User;

import java.util.List;

public interface ITaskService extends IAbstractVBSService<Task> {

    @NotNull
    List<Task> findAllByProjectId(@Nullable final User user, @Nullable final Project project) throws Exception;
}
