package ru.pyshinskiy.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {

    @WebMethod
    boolean isSessionExist(@Nullable final String id);

    @WebMethod
    @Nullable
    SessionDTO createSession(@Nullable final String login, @Nullable final String password) throws Exception;

    @WebMethod
    @Nullable
    SessionDTO updateSession(@Nullable final SessionDTO sessionDTO) throws Exception;

    @WebMethod
    void removeSession(@Nullable final String userId, @Nullable final String id) throws Exception;

    @WebMethod
    void removeAllSessions(@Nullable SessionDTO sessionDTO) throws Exception;

    @WebMethod
    void removeAllSessionsByUserId(@Nullable final String userId) throws Exception;
}
