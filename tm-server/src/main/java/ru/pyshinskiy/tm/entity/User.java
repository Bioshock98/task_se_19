package ru.pyshinskiy.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.dto.UserDTO;
import ru.pyshinskiy.tm.enumerated.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_user")
public final class User extends AbstractEntity {

    @Basic(optional = false)
    @Column(unique = true)
    @NotNull
    private String login;

    @Basic(optional = false)
    @NotNull
    private String passwordHash;

    @Basic(optional = false)
    @Enumerated(EnumType.STRING)
    @NotNull
    private Role role;

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Project> projects = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<Session> sessions = new ArrayList<>();

    @Nullable
    public static UserDTO toUserDTO(@Nullable final User user) {
        if(user == null) return null;
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setLogin(user.getLogin());
        userDTO.setPasswordHash(user.getPasswordHash());
        userDTO.setRole(user.getRole());
        return userDTO;
    }

    @NotNull
    public static List<UserDTO> toUsersDTO(@NotNull final List<User> users) {
        @NotNull final List<UserDTO> usersDTO = new ArrayList<>();
        for(User user : users) {
            @NotNull final UserDTO userDTO = new UserDTO();
            userDTO.setId(user.getId());
            userDTO.setLogin(user.getLogin());
            userDTO.setPasswordHash(user.getPasswordHash());
            userDTO.setRole(user.getRole());
            usersDTO.add(userDTO);
        }
        return usersDTO;
    }
}
