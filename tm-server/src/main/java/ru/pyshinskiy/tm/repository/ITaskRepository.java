package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.entity.User;

import java.util.List;

@Repository
public interface ITaskRepository extends CrudRepository<Task, String> {

    @NotNull
    List<Task> findTasksByUser(@NotNull final User user);

    @Nullable
    Task findTaskByUserAndId(@NotNull final User user, @NotNull final String id);

    @Nullable
    Task findTaskById(@NotNull final String id);

    @NotNull
    List<Task> findTasksByUserAndName(@NotNull final User user, @NotNull final String name);

    @NotNull
    List<Task> findTasksByUserAndDescription(@NotNull final User user, @NotNull final String description);

    void deleteTaskByUserAndId(@NotNull final User user, @NotNull final String id);

    void deleteTasksByUser(@NotNull final User user);

    @NotNull
    List<Task> findTasksByUserOrderByCreateTime(@NotNull final User user);

    @NotNull
    List<Task> findTasksByUserOrderByStartDate(@NotNull final User user);

    @NotNull
    List<Task> findTasksByUserOrderByFinishDate(@NotNull final User user);

    @NotNull
    List<Task> findTasksByUserOrderByStatus(@NotNull final User user);

    @NotNull
    List<Task> findTasksByUserAndProject(@NotNull final User user, @NotNull final Project project);
}
