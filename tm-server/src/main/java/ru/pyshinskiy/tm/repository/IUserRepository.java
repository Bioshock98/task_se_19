package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.pyshinskiy.tm.entity.User;

@Repository
public interface IUserRepository extends CrudRepository<User, String> {

    @Nullable
    User findUserById(@NotNull final String id);

    @Nullable
    User findUserByLogin(@NotNull final String login);

    boolean existsByLogin(@NotNull final String login);
}
