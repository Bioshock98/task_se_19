package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.entity.User;

import java.util.List;

@Repository
public interface ISessionRepository extends CrudRepository<Session, String> {

    @NotNull
    List<Session> findAllByUser(@NotNull final User user);

    @Nullable
    Session findSessionByUserAndId(@NotNull final User user, @NotNull final String id);

    @Nullable
    Session findSessionById(@NotNull final String id);

    void deleteByUserAndId(@NotNull final User user, @NotNull final String id);

    void deleteByUser(@NotNull final User user);

    void deleteAllByUser(@NotNull final User user);
}
