package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.User;

import java.util.List;

@Repository
public interface IProjectRepository extends CrudRepository<Project, String> {

    @NotNull
    List<Project> findProjectsByUser(@NotNull final User user);

    void deleteProjectsByUser(@NotNull final User user);

    @Nullable
    Project findProjectByUserAndId(@NotNull final User user, @NotNull final String id);

    @Nullable
    Project findProjectById(@NotNull final String id);

    @NotNull
    List<Project> findByUserAndName(@NotNull final User user, @NotNull final String name);

    @NotNull
    List<Project> findByUserAndDescription(@NotNull final User user, @NotNull final String description);

    void deleteByUserAndId(@NotNull final User user, @NotNull final String id);

    @NotNull
    List<Project> findProjectsByUserOrderByCreateTime(@NotNull final User user);

    @NotNull
    List<Project> findProjectByUserOrderByStartDate(@NotNull final User user);

    @NotNull
    List<Project> findProjectsByUserOrderByFinishDate(@NotNull final User user);

    @NotNull
    List<Project> findProjectsByUserOrderByStatus(@NotNull final User user);
}
