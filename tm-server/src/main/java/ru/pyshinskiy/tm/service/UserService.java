package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.pyshinskiy.tm.api.user.IUserService;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.repository.IUserRepository;

import java.util.List;

@Service
public class UserService extends AbstractService<User> implements IUserService {

    @Nullable
    @Autowired
    private IUserRepository userRepository;

    @Nullable
    @Override
    public User findOne(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("user id is empty or null");
        return userRepository.findUserById(id);

    }

    @Override
    @NotNull
    public List<User> findAll() {
        return (List<User>) userRepository.findAll();
    }

    @Transactional
    @Override
    public void persist(@Nullable final User user) throws Exception {
        if(user == null) throw new Exception("invalid user");
        userRepository.save(user);
    }

    @Transactional
    @Override
    public void merge(@Nullable final User user) throws Exception {
        if(user == null) throw new Exception("invalid user");
        userRepository.save(user);
    }

    @Transactional
    @Override
    public void remove(@Nullable final User user) {
        userRepository.delete(user);
    }

    @Transactional
    @Override
    public void removeAll() {
        userRepository.deleteAll();
    }

    @Override
    @Nullable
    public User getUserByLogin(@Nullable String login) throws Exception {
        if(login == null || login.isEmpty()) return null;
        return userRepository.findUserByLogin(login);
    }

    @Override
    public boolean isUserExistByLogin(@Nullable final String login) throws Exception {
        if(login == null) throw new Exception("login is invalid");
        return userRepository.existsByLogin(login);
    }
}
