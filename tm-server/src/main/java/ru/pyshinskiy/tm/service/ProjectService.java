package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.repository.IProjectRepository;

import java.util.List;

@Service
public class ProjectService extends AbstractWBSService<Project> implements IProjectService {

    @Autowired
    @NotNull
    private IProjectRepository projectRepository;

    @Nullable
    @Override
    public Project findOneByUserId(@Nullable final User user, @Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("project id is empty or null");
        if(user == null) throw new Exception("user is null");
        return projectRepository.findProjectByUserAndId(user, id);
    }

    @Override
    @NotNull
    public List<Project> findAllByUserId(@Nullable final User user) throws Exception {
        if(user == null) throw new Exception("user is null");
        return projectRepository.findProjectsByUser(user);
    }

    @Transactional
    @Override
    public void removeByUserId(@Nullable final User user, @Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("project id is empty or null");
        if(user == null) throw new Exception("user is null");
        projectRepository.deleteByUserAndId(user, id);
    }

    @Transactional
    @Override
    public void removeAll(@Nullable final User user) throws Exception {
        if(user == null) throw new Exception("user is null");
        projectRepository.deleteProjectsByUser(user);
    }

    @Override
    @NotNull
    public List<Project> findByName(@Nullable final User user, @Nullable final String name) throws Exception {
        if(user == null) throw new Exception("user is null");
        if(name == null || name.isEmpty()) throw new Exception("name is empty or null");
        return projectRepository.findByUserAndName(user, name);
    }

    @Override
    @NotNull
    public List<Project> findByDescription(@Nullable final User user, @Nullable final String description) throws Exception {
        if(user == null) throw new Exception("user is null");
        if(description == null || description.isEmpty()) throw new Exception("description is empty or null");
        return projectRepository.findByUserAndDescription(user, description);
    }

    @Transactional
    @Override
    @NotNull
    public List<Project> sortByCreateTime(@Nullable final User user) throws Exception {
        if(user == null) throw new Exception("user is null");
        return projectRepository.findProjectsByUserOrderByCreateTime(user);
    }

    @Transactional
    @Override
    public @NotNull List<Project> sortByStartDate(@Nullable final User user) throws Exception {
        if(user == null) throw new Exception("user is null");
        return projectRepository.findProjectByUserOrderByStartDate(user);
    }

    @Transactional
    @Override
    @NotNull
    public List<Project> sortByFinishDate(@Nullable final User user) throws Exception {
        if(user == null) throw new Exception("user is null");
        return projectRepository.findProjectsByUserOrderByFinishDate(user);
    }

    @Transactional
    @Override
    @NotNull
    public List<Project> sortByStatus(@Nullable final User user) throws Exception {
        if(user == null) throw new Exception("user is null");
        return projectRepository.findProjectsByUserOrderByStatus(user);
    }

    @Override
    @Nullable
    public Project findOne(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("project id is empty or null");
        return projectRepository.findProjectById(id);
    }

    @Override
    @NotNull
    public List<Project> findAll() {
        return (List<Project>) projectRepository.findAll();
    }

    @Transactional
    @Override
    public void persist(@Nullable final Project project) throws Exception {
        if(project == null) throw new Exception("invalid project");
        projectRepository.save(project);
    }

    @Transactional
    @Override
    public void merge(@Nullable final Project project) throws Exception {
        if(project == null) throw new Exception("invalid project");
        projectRepository.save(project);
    }

    @Transactional
    @Override
    public void remove(@Nullable final Project project) throws Exception {
        if(project == null) throw new Exception("project is null");
        projectRepository.delete(project);
    }

    @Transactional
    @Override
    public void removeAll() {
        projectRepository.deleteAll();
    }
}
