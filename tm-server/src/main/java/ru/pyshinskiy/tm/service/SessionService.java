package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.pyshinskiy.tm.api.session.ISessionService;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.repository.ISessionRepository;

import java.util.List;

@Service
public class SessionService extends AbstractService<Session>  implements ISessionService {

    @Autowired
    @NotNull
    private ISessionRepository sessionRepository;


    @Override
    public boolean isSessionExist(@Nullable final String id) {
            return sessionRepository.existsById(id);
    }

    @Transactional
    @Override
    @Nullable
    public Session findOneByUserId(@Nullable final User user, @Nullable final String id) throws Exception{
        if(user == null) throw new Exception("user is null");
        if(id == null) throw new Exception("invalid session id");
        return sessionRepository.findSessionByUserAndId(user, id);
    }

    @Transactional
    @Override
    public void removeByUserId(@Nullable final User user, @Nullable final String id) throws Exception {
        if(user == null) throw new Exception("user id null");
        if(id == null) throw new Exception("invalid session id");
        sessionRepository.deleteByUserAndId(user, id);
    }

    @Transactional
    @Nullable
    @Override
    public Session findOne(@Nullable final String id) throws Exception {
        if(id == null) throw new Exception("invalid session id");
        return sessionRepository.findSessionById(id);
    }

    @Transactional
    @Override
    @NotNull
    public List<Session> findAll() {
        return (List<Session>) sessionRepository.findAll();
    }

    @Transactional
    @Override
    @NotNull
    public List<Session> findAllByUserId(@NotNull final User user) {
        return sessionRepository.findAllByUser(user);
    }

    @Transactional
    @Override
    public void persist(@Nullable final Session session) throws Exception {
        if(session == null) throw new Exception("invalid session");
        sessionRepository.save(session);
    }

    @Transactional
    @Override
    public void merge(@Nullable final Session session) throws Exception {
        if(session == null) throw new Exception("invalid session");
        sessionRepository.save(session);
    }

    @Transactional
    @Override
    public void remove(@Nullable final Session session) throws Exception {
        if(session == null) throw new Exception("session is null");
        sessionRepository.delete(session);
    }

    @Transactional
    @Override
    public void removeAll() {
        sessionRepository.deleteAll();
    }

    @Transactional
    @Override
    public void removeAllByUserId(@Nullable final User user) throws Exception {
        if(user == null) throw new Exception("user is null");
        sessionRepository.deleteByUser(user);
    }
}
