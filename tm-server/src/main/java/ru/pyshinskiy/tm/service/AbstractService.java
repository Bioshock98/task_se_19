package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.service.IService;
import ru.pyshinskiy.tm.entity.AbstractEntity;

import java.util.List;

public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    @Nullable
    @Override
    abstract public T findOne(@Nullable final String id) throws Exception;

    @NotNull
    @Override
    abstract public List<T> findAll() throws Exception;

    @Override
    abstract public void persist(@Nullable final T t) throws Exception;

    @Override
    abstract public void merge(@Nullable final T t) throws Exception;

    @Override
    abstract public void remove(@Nullable final T t) throws Exception;

    @Override
    abstract public void removeAll() throws Exception;
}
