package ru.pyshinskiy.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.repository.ITaskRepository;

import java.util.List;

@Service
public class TaskService extends AbstractWBSService<Task> implements ITaskService {

    @Autowired
    @NotNull
    private ITaskRepository taskRepository;

    @Nullable
    @Override
    public Task findOneByUserId(@Nullable final User user, @Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("task id is empty or null");
        if(user == null) throw new Exception("user is null");
        return taskRepository.findTaskByUserAndId(user, id);
    }

    @Override
    @NotNull
    public List<Task> findAllByUserId(@Nullable final User user) throws Exception {
        if(user == null) throw new Exception("user is null");
        return taskRepository.findTasksByUser(user);
    }

    @Transactional
    @Override
    public void removeByUserId(@Nullable final User user, @Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("task id is empty or null");
        if(user == null) throw new Exception("user is null");
        taskRepository.deleteTaskByUserAndId(user, id);
    }

    @Transactional
    @Override
    public void removeAll(@Nullable final User user) throws Exception {
        if(user == null) throw new Exception("user is null");
        taskRepository.deleteTasksByUser(user);
    }

    @Override
    @NotNull
    public List<Task> findByName(@Nullable final User user, @Nullable final String name) throws Exception {
        if(user == null) throw new Exception("user is null");
        if(name == null || name.isEmpty()) throw new Exception("name is empty or null");
        return taskRepository.findTasksByUserAndName(user, name);
    }

    @Override
    @NotNull
    public List<Task> findByDescription(@Nullable final User user, @Nullable final String description) throws Exception {
        if(user == null) throw new Exception("user is null");
        if(description == null || description.isEmpty()) throw new Exception("description is empty or null");
        return taskRepository.findTasksByUserAndDescription(user, description);
    }

    @Transactional
    @Override
    @NotNull
    public List<Task> sortByCreateTime(@Nullable final User user) throws Exception {
        if(user == null) throw new Exception("user is null");
        return taskRepository.findTasksByUserOrderByCreateTime(user);
    }

    @Transactional
    @Override
    public @NotNull List<Task> sortByStartDate(@Nullable final User user) throws Exception {
        if(user == null) throw new Exception("user is null");
        return taskRepository.findTasksByUserOrderByStartDate(user);
    }

    @Transactional
    @Override
    @NotNull
    public List<Task> sortByFinishDate(@Nullable final User user) throws Exception {
        if(user == null) throw new Exception("user is null");
        return taskRepository.findTasksByUserOrderByFinishDate(user);
    }

    @Transactional
    @Override
    @NotNull
    public List<Task> sortByStatus(@Nullable final User user) throws Exception {
        if(user == null) throw new Exception("user is null");
        return taskRepository.findTasksByUserOrderByStatus(user);
    }

    @Override
    @Nullable
    public Task findOne(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("task id is empty or null");
        return taskRepository.findTaskById(id);
    }

    @Override
    @NotNull
    public List<Task> findAll() {
        return (List<Task>) taskRepository.findAll();
    }

    @Transactional
    @Override
    public void persist(@Nullable final Task project) throws Exception {
        if(project == null) throw new Exception("invalid task");
        taskRepository.save(project);
    }

    @Transactional
    @Override
    public void merge(@Nullable final Task project) throws Exception {
        if(project == null) throw new Exception("invalid task");
        taskRepository.save(project);
    }

    @Transactional
    @Override
    public void remove(@Nullable final Task project) throws Exception {
        if(project == null) throw new Exception("project is null");
        taskRepository.delete(project);
    }

    @Transactional
    @Override
    public void removeAll() {
        taskRepository.deleteAll();
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@Nullable final User user, @Nullable final Project project) throws Exception {
        if(user == null || project == null) throw new Exception("one of the parameters passed is zero");
        return taskRepository.findTasksByUserAndProject(user, project);
    }
}
