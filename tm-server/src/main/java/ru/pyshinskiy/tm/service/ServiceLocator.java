package ru.pyshinskiy.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.project.IProjectService;
import ru.pyshinskiy.tm.api.service.IServiceLocator;
import ru.pyshinskiy.tm.api.session.ISessionService;
import ru.pyshinskiy.tm.api.task.ITaskService;
import ru.pyshinskiy.tm.api.user.IUserService;

@Getter
@Setter
@Component
public final class ServiceLocator implements IServiceLocator {

    @Autowired
    @Nullable
    private IProjectService projectService;

    @Autowired
    @Nullable
    private ITaskService taskService;

    @Autowired
    @Nullable
    private ISessionService sessionService;

    @Autowired
    @Nullable
    private IUserService userService;
}
