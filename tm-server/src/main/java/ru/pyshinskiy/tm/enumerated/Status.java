package ru.pyshinskiy.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Status {

    PLANNED("PANNED"),
    IN_PROGRESS("IN PROGRESS"),
    DONE("DONE");

    @NotNull
    private String name;

    Status(@NotNull final String name) {
        this.name = name;
    }

    @NotNull
    public String displayName() {
        return name;
    }
}
