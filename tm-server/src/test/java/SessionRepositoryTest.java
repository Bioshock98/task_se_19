import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;
import ru.pyshinskiy.tm.config.ApplicationConfig;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.enumerated.Role;
import ru.pyshinskiy.tm.repository.ISessionRepository;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = { ApplicationConfig.class },
        loader = AnnotationConfigContextLoader.class)
public class SessionRepositoryTest {

    @Resource
    private ISessionRepository sessionRepository;

    @Test
    public void saveSession() {
        @NotNull final Session session = new Session();
        session.setRole(Role.TEST);
        sessionRepository.save(session);
        assertEquals(session.getId(), sessionRepository.findSessionById(session.getId()).getId());
    }

    @Test
    public void deleteSession() {
        @NotNull final Session session = new Session();
        session.setRole(Role.TEST);
        sessionRepository.save(session);
        sessionRepository.delete(session);
        assertFalse(sessionRepository.existsById(session.getId()));
    }
}
