import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;
import ru.pyshinskiy.tm.config.ApplicationConfig;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.repository.ITaskRepository;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = { ApplicationConfig.class },
        loader = AnnotationConfigContextLoader.class)
public class TaskRepositoryTest {

    @Resource
    private ITaskRepository taskRepository;

    @Test
    public void saveTask() {
        @NotNull final Task task = new Task();
        task.setName("TESTH2PROJECT");
        taskRepository.save(task);
        assertEquals(task.getId(), taskRepository.findTaskById(task.getId()).getId());
    }

    @Test
    public void deleteTask() {
        @NotNull final Task task = new Task();
        taskRepository.save(task);
        taskRepository.delete(task);
        assertFalse(taskRepository.existsById(task.getId()));
    }
}
