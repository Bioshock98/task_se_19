import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;
import ru.pyshinskiy.tm.config.ApplicationConfig;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.enumerated.Role;
import ru.pyshinskiy.tm.repository.IUserRepository;

import javax.annotation.Resource;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = { ApplicationConfig.class },
        loader = AnnotationConfigContextLoader.class)
public class UserRepositoryTest {

    @Resource
    private IUserRepository userRepository;

    @Test
    public void saveUser() {
        @NotNull final User user = new User();
        user.setLogin("test" + new Random().toString());
        user.setPasswordHash(new Random().toString());
        user.setRole(Role.TEST);
        userRepository.save(user);
        assertEquals(user.getId(), userRepository.findUserById(user.getId()).getId());
    }

    @Test
    public void deleteUser() {
        @NotNull final User user = new User();
        user.setLogin("test" + new Random().toString());
        user.setPasswordHash(new Random().toString());
        user.setRole(Role.TEST);
        userRepository.save(user);
        userRepository.delete(user);
        assertFalse(userRepository.existsById(user.getId()));
    }
}
