import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;
import ru.pyshinskiy.tm.config.ApplicationConfig;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.repository.IProjectRepository;

import javax.annotation.Resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = { ApplicationConfig.class },
        loader = AnnotationConfigContextLoader.class)
public class ProjectRepositoryTest {

    @Resource
    private IProjectRepository projectRepository;

    @Test
    public void saveProject() {
        @NotNull final Project project = new Project();
        project.setName("TESTH2PROJECT");
        projectRepository.save(project);
        assertEquals(project.getId(), projectRepository.findProjectById(project.getId()).getId());
    }

    @Test
    public void deleteProject() {
        @NotNull final Project project = new Project();
        projectRepository.save(project);
        projectRepository.delete(project);
        assertFalse(projectRepository.existsById(project.getId()));
    }
}
